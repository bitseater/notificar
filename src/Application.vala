/*
* Copyright (c) 2023 Carlos Suárez (http://gitlab.com/bitseater)
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; if not, write to the
* Free Software Foundation, Inc., 59 Temple Place - Suite 330,
* Boston, MA 02111-1307, USA.
*
* Authored by: Carlos Suárez <bitseater@gmail.com>
*/
public class MyApp : Adw.Application {

    public MyApp () {
        application_id = "com.gitlab.bitseater.notificar";
    }

    protected override void activate () {
        var main_window = new Adw.ApplicationWindow (this) {
            default_height = 300,
            default_width = 300,
            title = "Hello World"
        };
        var title_label = new Gtk.Label ("Notifications");
        var show_button = new Gtk.Button.with_label ("Show");

        var box = new Gtk.Box (Gtk.Orientation.VERTICAL, 1);
        box.set_spacing (6);
        box.set_margin_start (10);
        box.set_margin_end (10);
        box.append (title_label);
        box.append (show_button);
        var ejemplo = new Gtk.Button ();
        box.append (ejemplo);
        main_window.content = box;
        main_window.present ();

        var quit_action = new SimpleAction ("quit", null);
        add_action (quit_action);
        quit_action.activate.connect (quit);

        show_button.clicked.connect (() => {
        var notification = new Notification ("Hello World");
            notification.set_body ("This is my first notification!");
            notification.add_button ("Quit", "app.quit");
            notification.set_priority (NotificationPriority.HIGH);
            send_notification ("com.gitlab.bitseater.notificar", notification);
        });
    }

    public static int main (string[] args) {
        return new MyApp ().run (args);
    }
}
